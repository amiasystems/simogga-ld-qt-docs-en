Creating of the As-IS situation in the **real view**
======================================================

.. include:: image.rst
.. include:: icon.rst

|quick_start_2|


Opening a file
----------------------------------

Opening the xml file starts in the ‘real’ view. The machines are placed in a tray. They may be placed on the factory drawing only once the working areas (yellow cell) are defined.
 
|open_file|

When opening a file, a dialog box appears asking whether SIMOGGA must create locations for Raw Material (MP) and Finished Part (FP). If the data already includes these inputs and outputs of the system, click on "No".

For the TrialCase, click on "Yes".  The representation of the flow is more complete if we take into account the inputs and outputs of the system. 

|message_load_case|

.. hint:: If the user loads a case that has already been saved with raw materials MP / PF finished parts, they will appear even if the user chooses not to create them. As it has already been created previously, they are now part of machines covered by this case.

If SIMOGGA fails to load the selected XML file, an error window will appear to inform to the user. For SIMOGGA to able to load an xml file, the structure of it should be the structure set up by SIMOGGA.

|message_dialog_openfail|

Check that the xml file does not contain special characters such as:

* #DIV/0!

* #N/A

* #VALEUR!

* /

* \*

* '

* (  or  )

* &


Saving a case
----------------------------------

Two types of save options can be found in the "File" menu:

* Save: The backup will be done on the open file. The changes will be saved in the same xml file.

|menu_file_save|

* Save As ...: The user can choose the backup location and name of the saved file.

|menu_file_save_as|

.. hint:: Don't forget to save your work regularly to avoid any lost.


Create the factory - Design mode
----------------------------------

In the real view, it is possible to obtain a two-dimensional representation of the work area corresponding to the reality.

* Click on Scenario 1 (base)

|scenario|

* In "Real View", click on S1RV1 - RealAlternative1

* Save this alternative as the reference: Right click on the alternative, select "Set / Unset as Baseline Alternative" option. This will serve as a benchmark to compare all scenarios. 

|set_as_is_situation|

Or via the scenario menu: 

|set_as_is_situation_by_scenario|

* Select the "Design Mode" from the drop-down list

|design_selection|

* The machine box is not visible. A reference machine appears in the left top. This machine represents the minimum size of all the machines.



|insert_factory_plan| Insert a factory plan as a background image
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Insert the plan

	* Click on the button |insert_factory_plan| 

	* A windows open

	* Sélectionner un plan d’usine au format png ou jpg

	* Cliquer sur Ok

* Adapt the plan

	* Move the plan if need with the mouse

	* Click on the image to activate it

	* Adapt the size of the map (via the zoom with the mouse wheel) 

	* The size of the reference machine must be the same than the machine on the plan

	.. ::hint It is the plan that be adapted to the machine reference. The size of this machine can be modified.

	* To exit of this adaptation, click on "esc" on the keyboard or on the button |move|


|insert_factory_border| Draw the outline of the factory on the plan 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Info : Sidebar navigation

	|panel_lateral_structure_select_comp|

	* Click on the icon |insert_factory_border|

	* Create the outline by simple successive clicks on the corners of the map. Every click is a point on the polygon representing the outline.

	* End with a double-click (on the penultimate point) to indicate that the polygon is complete and it will close automatically.

	* Choose the type of desired zone (site, building, floor, area)

	* Rename the area

|process_real_2_factory|


|insert_cell_border|  Identify sub-areas, if necessary 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	* **Select the parent zone** via a click on the area at the scene or at the navigation menu in the structure.

	|panel_lateral_structure_select_site|

	* Click on the icon |insert_factory_border|

	* Create the outline by simple successive clicks on the corners of the map. Every click is a point on the polygon representing the outline.

	* End with a double-click (on the penultimate point) to indicate that the polygon is complete and it will close automatically. 

	* Choose the type of desired zone (site, building, floor, area)

	* Rename the area

	* Automatic truncation of the new area relative to the parent zone. It is therefore not necessary to be precise in the drawing at the boundaries of the parent zone. 


|insert_cell_border|  Identify work areas where machines may be placed (cell) 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les machines ne pourront être placées que dans ces zones pour être connectées. 

	* Select the parent zone via a click on the area at the scene or at the navigation menu in the structure. An active zone is green.

	|panel_lateral_structure_select_site|
	
	* Click on the icon |insert_cell_border|
	
	* * If the parent zone is not selected, a warning message appears. 
	
	|message_parent_selection|

	.. hint:: We must then select the parent in the side navigation menu. The scene is not accessible until the button |insert_cell_border| is pressed.

	.. hint:: Pour désactiver n’importe quelle fonctionnalité, appuyer sur « esc ».

	* Create a polygon using a series of mouse clicks 

	* Finish with a double click to mark the automatic closing of the polygon 

	* If the polygon created exceeds the area in which it is located, it will be truncated to fit the contour. 

|process_real_3_cell|


.. hint:: Les machines ne pourront être placées que sur ces zones cellules (jaune).   

.. hint::hold the Ctrl / Cmd to create multiple zones without having to click the icon if every time button.



|scale|  To scale 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	* Click on the button  |scale|

	* Use a distance that is defined on the plan.

	* Click (single click) on the two points defining the distance.

	|set_scale|

	* Set the distance in meters

	* Click OK



Create the skeleton of aisles
----------------------------------

The skeleton is the graphical of view the aisles of the factory through which will pass the traffic between the work areas (intercellular traffic). This tool is available in "design mode" of the real alternative.

* Select the area where you want to put aisles using the scene or the navigation menu

* Click the icon  |insert_skeleton_point| 
	
* Insert the junction of aisles with double clicks.

* SIMOGGA automatically connects the points if:

	* The line does not cross working areas

	* line does not go too near the corners of working areas.

	* The lines are not too close.

|process_real_4_skeleton|

* Add lines manually

	* Activate the button  |insert_edge|

	* Double click on two points to connect


* Remove unwanted lines

	* Select the element to be deleted

	* Choose several elements holding down the Ctrl / Cmd keys

	* Click on the button  |remove_any_element|
 
	* Click OK

.. It is also possible to generate the skeleton fully automated manner using the "create automatic skeleton" button on the toolbar. If this option is chosen, SIMOGGA attempt to generate a skeleton covering all the intercell areas.

* Add inputs and outputs (IO points) on the edge of an area.
	
	|process_real_5_io|

	* Activate the button  |input|

	* Double click the edge of the area.

	* Right-click the item added to change its type
		* IN :Only inflows can pass
		* OUT : Only outflows can pass
		* I/O : All the flows can pass. 


.. hint:: Les points IO seront utiles dans le cas où on se trouve face à une usine comportant plusieurs étages et/ou bâtiments, ou si l’on souhaite travailler sur plusieurs sites simultanément. 
 
* Add aisles in the cells

	* Click on button  |insert_edge|

	* Double-click in the IO in the cell, and double-click on the next point. 

		- If a point exists, it will be connected to the previous point
		- Otherwise, a new point is created

	* Do it again until the aisle is completed in the cell


* Set a distance on a skeleton line.
		
		.. hint:: This will be particularly useful when you want to work with multiple sites simultaneously, a single skeleton line maybe set in meters or kilometers.
		
		|process_real_7_setvalue|

		* Right click on the line to set

		* Select "Set the distance"

		* Enter a value in meters or kilometers

		* Right-click the line to display the entered value

		* Delete the value via the "Reset" option


Validation du design
------------------------------------------------------------

Each crosspoint of the skeleton must be with a border red. 

|skeletonpoint_connected|

If the circle is completly red, the crosspoint is alone and not connected to the graph.

|skeletonpoint_notconnected|

To check the connections, click on the button |nodes|.

Each skeleton point is represented with a red point and small blue lines. These blue lines show the connection to the graph.

|skeletonpoint_valide|

When switching in the "Interaction mode", a message appear if the graph presents some connection default as an isolated point.

In the lateral panel "Info", the liste of used machines is red represent all the machines not connected to the graph.

|panel_lateral_machine_notconnected|


Positioning the machines – Interaction Mode
------------------------------------------------------------

When work areas (yellow) were created in design mode, machines can be placed. To do this, a simply "drag & drop" the machines. We can therefore represent the initial situation in which the plant is located.

Positioning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Select the "Interaction Mode" in the drop-down list

|interaction_selection|

* Déplacer les machines du BAC vers les zones cellules dans la position actuelle de l’usine 

	* En cliquant sur CTRL/CMD on peut sélectionner plusieurs machines

	* Les machines seront automatiquement connectées au graphe d’allées. 

	* Si il existe un flux entre deux machines positionnées sur le plan, il apparaitra automatiquement.


Different type of views are available
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	* |visualisation_standard|  standard : Machines are connected directly if they belong to the same area, if not traffic is through the aisles

	* |visualisation_byalleys|  By the aisles only : All traffic via the aisles.

	* |visualisation_crowfly|  As the bird flies :the aisle backbone is not used. The machines are connected via directional flow.


.. hint:: In the first two displays, the concept of direction disappears. It is all the traffic that is shown on each segment. This type of view mode is only possible if an aisle skeleton was built in design mode. Otherwise, the representation is as the crow flies.

Machine interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The machines can be modified by:

* The position with "drag & drop".

* Their size to represent more accurately the real place that takes the machine within the factory:

|process_real_9_machine_resize|

	* Select the machine 

	* Display of the squares on the contour 

	* Move the squares of a contour
 
	* leur orientation :
	
	* Click on the "Rotation" button |rotate_machine|

	* Double click on the machine.

* Immobilisation. A locked machine canot be moved:
	
	|process_real_10_machine_lock|

	* Click on the button |lock_machine|

	* Double click on the machine.

	* The lock is displayed on the machine.


KPIs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* When all machines are in position, the performance indicators assess the situation quickly

	* Number of kilometers traveled;

	* Travel time

	* Cost of transport

|dashboard|




