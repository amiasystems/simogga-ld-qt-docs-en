Excel file
======================

.. include:: image.rst
.. include:: icon.rst

|quick_start_1|

To collect the data from the customer and to simplify the completion of the xml, an excel file is used: `AMIA-SIMOGGA-SampleCase.xlsm <http://www.amia-systems.com/uploads/8/3/6/7/8367487/1.amia-simogga-samplecase.xlsm>`_

The excel file with the data for the Trial Version can be download `here <https://www.amia-systems.com/wp-content/uploads/2020/06/AMIA-SIMOGGA-XLS2XML.zip>`_

This excel file is composed by 5 sheets. The three firsts explain how to use the file. The sheet "Sample_Data" contains three tables to fill with the data from your ERP. The last sheet give you the visual process represented by the data.

Per the instructions provided in the excel file, only the columns highlighted in yellow are required to be completed. All of the other columns are for information to enable a more in-depth analysis (such as machine load and capacity).


Table of Products
-----------------

|products|

In the table of products, each row corresponds to a stage of the process required to make the finished part. 
The sequence the stages are inserted on the file for each product corresponds to the actual production sequence.
The required information for each stage of the production process for a part is:

* Product code

* Quantity to be produced

* The code of the machine to perform the stage of the process. 

* The size of the transfer load between each stage of the process. (This is usually depends on the mode of transport and the type of packaging). 


Table of Machines
----------------------------------

|machines|

The table of machines contains all of the information regarding machines, workstations and storage areas. 
The required inputs are:

* The machine codes used by each product 

* The type of the machine: each machine belongs to one or more type of machine depending on the function and operation it can perform. 


Table of Machine Type
----------------------------------

|machine_type|

The table of Machine Type lists the different types included in the machine table


Conversion
-----------------

A macro converts the Excel file into xml file readable by SIMOGGA.

Via the main menu of Excel : Tools/Macro/Macros, Run the macro "ConvertToXMLFile".

A file « SIMOGGA-output.xml » is created in the repository beginning by XLS2XML (at the same level as the excel file).


