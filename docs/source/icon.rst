.. file with all image icons to include
 
.. design
.. |insert_factory_plan| image:: _static/img/icons/insert_background_en.png
.. |insert_factory_border| image:: _static/img/icons/insert_factory_border_en.png
.. |insert_cell_border| image:: _static/img/icons/insert_cell_border_en.png
.. |insert_skeleton_point| image:: _static/img/icons/design_en.png
.. |insert_edge| image:: _static/img/icons/line_en.png
.. |insert_io| image:: _static/img/icons/output_en.png
.. |scale| image:: _static/img/icons/scale_en.png
.. |nodes| image:: _static/img/icons/nodes_en.png
.. |connect| image:: _static/img/icons/connect_en.png

.. |remove_any_element| image:: _static/img/icons/delete_en.png

.. machine
.. |lock_machine| image:: _static/img/icons/lock_en.png
.. |rotate_machine| image:: _static/img/icons/rotate_en.png
.. |multiple_selection| image:: _static/img/icons/selectMultiple_en.png
.. |delete_item| image:: _static/img/icons/delete_en.png

.. cell
.. |insert_virtual_cell| image:: _static/img/icons/virtualCell_en.png
.. |removeMachineFromVirtualCell| image:: _static/img/icons/removeMachineFromVirtualCell_en.png
.. |table_of_families| image:: _static/img/icons/table_en.png
.. |input| image:: _static/img/icons/output_en.png
.. |output| image:: _static/img/icons/output_en.png

.. general view
.. |move| image:: _static/img/icons/select_en.png
.. |hide_background| image:: _static/img/icons/hide_background_en.png
.. |panel_machine| image:: _static/img/icons/store_en.png
.. |empty_machine| image:: _static/img/icons/machine_en.png
.. |connect_machine| image:: _static/img/icons/connect_en.png
.. |prevent_collision| image:: _static/img/icons/collision_en.png

.. |visualisation_type| image:: _static/img/icons/type_visualization_en.png
.. |visualisation_byalleys| image:: _static/img/icons/byalleys_en.png
.. |visualisation_crowfly| image:: _static/img/icons/crowfly_en.png
.. |visualisation_standard| image:: _static/img/icons/standard_en.png

