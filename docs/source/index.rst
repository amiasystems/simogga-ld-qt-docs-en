.. SIMOGGA LD Qt documentation master file, created by
   sphinx-quickstart on Tue Aug 26 13:57:17 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SIMOGGA LD Qt's documentation!
=========================================

SIMOGGA, is an innovative graphical material handling system that optimizes factory layout design based on a global flow analysis.

SIMOGGA relies on an optimization core resulting from 10 years of intensive research and on an intelligent interface. It is user friendly and requires a small amount of data to deliver results. 
SIMOGGA guides the users through the process of improvement, but does not replace them. It feeds on the knowledge and experience collected from plant managers and operators and any other key players in the improvement process.

SIMOGGA key concepts

* The visualization of the results gives you concret elements to discuss
* The quantification of the results allows to justify your proposition
* The interactivity of the tool gives you the opportunity to test any scenarios and use it during the meetings
* The simplicity of use permits to include all actors concerned by the change
* The intelligence of the tool guides you in your approach

For the trial version, follow the process from the section : `Creating of the As-IS situation <http://simogga-ld-qt-en.readthedocs.org/en/latest/create_asis.html>`_ in the **real view**

Good work

.. note:: Don't forget that you can contact us at anytime via this page: http://www.amia-systems.com/contact


Contents:

.. toctree::
   :maxdepth: 2
   
   prerequisite
   prise_en_main_rapide
   excel_file
   create_asis
   flow_analysis
   layout_generation
   datapanel
   menu_principal
   menu_vues
   menu_sur_vues
   menu_lateral
   menu_file_box
   traitement_cas_complet
   scheduler
   scheduler_result
   glossary
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex`