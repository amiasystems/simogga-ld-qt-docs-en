Generate scenarios 
======================================================

.. include:: image.rst
.. include:: icon.rst

|quick_start_4|


How to use the scenarios and alternatives?
----------------------------------------------------

Based on the situation AS-IS and analysis of previous flows, it is possible to create all possible scenarios.  

.. ::hint A secnario has a unique design, a set of defined machines and a flow matrix (a routing solution where each operation is assigned to a machine).

A alternative is used to test the location of teh machines.

A scenario is used to test a new design (new working area, new aisles...), solutions with new investments in term of machines or new routings for the parts.


Modify the location of the machines in the As-Is sitiation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Duplicate « Alternative AS-IS »

* Changing the positions of the machines has a direct impact on the mileage (in green if a gain is observed if not red)

|evaluation_positive|

|evaluation_negative|

New scenario based on the rerouting of flows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To create solutions based on the rerouting of flows performed in the graphical view of the "Scenario 2":

* Work in Scenario 2 alternative "S2RA1"

* Determine incremental changes (in terms of positioning of machines):
	* Modify « S2RA1 »
	* Duplicate« S2RA1 » en « S2RA2 »
	* Modify « S2RA2 »
	* Duplicate « S2RA2 » en « S2RA3 »
	* Modify « S2RA3 » 

..hint each stage to be saved must be duplicated in order to keep the changes.

* Modify the design of the factory
	* Duplicate Scenario 2 (Scenario 3 =)
	* Modify the design (delete cells, create new cells ...)
	* Delete the non-useful alternatives from the previous Scenario 2 ("S2RA1" and   "S2RA2")
	* Start to change the alternative "S3RA3" (new design with machines positioned as the last alternative scenario 2)
	* Proceed with incremental changes to create a different alternative "S3RA3", "S3RA4" to "S3RA5"

Incremental changes (via alternatives) can result in freeing up of an area so as to facilitate redesign. (via a new scenario).


Using the Dashboard
------------------------------
|process_real_11_dashboard|

The dashboard allows you to compare different real alternatives to quickly and easily visualize which is the best of them. It contains several data:

* The name of the alternative and it's scenario .

* Total cost that represents the situation

* The percentage gain this situation than that indicated as the reference

* The total number of kilometers traveled by products

* The total time

To determine what is the alternative reference, you either go through the Scenario menu, or right-click on the selected alternative and choose "set as baseline" option. Computing the winning percentage of alternatives will be based on this alternative reference.


Fermeture
------------------------------

|menu_file_close|

When closing the opened case with the command "Close" in the menue "File", it will first ask it a backup of any changes made to the current case is necessary.

|message_close|

If the response is "Yes", the modification will be saved in the original file xml. On the contrary, the modification will be lost. 
