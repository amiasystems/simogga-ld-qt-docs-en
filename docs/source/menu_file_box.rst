Main Menu
========================

.. include:: image.rst
.. include:: icon.rst

Options / Settings
-------------------

User Settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|box_parameters|
 
* **View with the IO** : Determines whether the raw materials / finished products must be taken into account in the flow graph.

* **View with the IO of the cells** : For the cell view, it determines whether the graphical flow must go via the input / output points.

* **Connect the graph** : Determines whether the machines must be connected to the graph. This is useful for locating all the machines on a very complex factory level, for which the analysis is too heavy.

* **The raw material MP / PF finished parts must be created by SIMOGGA** : Determines whether the raw materials / finished products must be created when opening a case. If they were added, and the file was saved, SIMOGGA doesn't create more.

* **Traffic visualization** : Determines the type of visualization to display the desired traffic. Three types are available:

	* **Standard** : Determines whether flows pass only the aisles (more intracellular connection between the machines).
	* **Via the aisles** : The machines will automatically interconnected by aisles. There will be no intracellular with the machines.
	* **As the bird flies** : The machines will be connected by the shortest distance as the crow flies.

* **Proportional flow** : Determines if the thickness of the flow shown proportionate.

* **Transport speed** : Determines the speed of transport of goods within the factory.

* **Scale** :Indicates the scale of the plan. To change, it is necessary to go through the design mode of the actual view.

* **Cost per hour** : Determines the cost per hour of transportation within the plant.

* **Type of traffic** : Determines the type of flow represented. The flow may be:

	* The number of product transactios: amount of trips made.
	* The number of products transferred: the amount of transferred items.

* **Size of the machines** : Determines the default size of the machines.

* **Maximum thickness** : Determines the maximum thickness of the flows.

* **Minimum thickness** : Determines the minimum thickness of the flows.


ADMIN TAB
~~~~~~~~~~~~~~~~
 


Rerouting
----------------
 

Rerouting allows reassigning operations performed by one machine to another machine of the same type. 

.. hint:: Create a new scenario to compare the baseline with the new solution.

|scenario_right_click|  

or

|menu_scenario_del_sce|

Changes made the rerouting are applied to all alternatives of the current scenario. Reminder, the characteristics of a scenario are:

* The number of machines available
* A routing solution (assignment of operations on machines defining traffic / flow between machines)  
* A layout in the real view

The alternatives are used for any change of the position of the machines.

When you want to reroute in the baseline scenario, a warning message appears:

|message_reroutage_base_case|

To do so, it is necessary to proceed as follows:

* Duplicate the baseline scenario.

* Open the rerouting tool via the menu.

|box_reroutage_open|

* Select the machine on which the operations are being carried out. 

|box_reroutage_selection_old|

* Select the new machine upon which the flows must pass (this will be in general a machine having been duplicated). 

|box_reroutage_selection_new|

* Select the machine(s) from where the products are to be reassigned (Origin).

|box_reroutage_selection_from|

* Select the machine(s) to which the products are to be reassigned (Destination).

|box_reroutage_selection_to|

	* These two machines are dependent on one another. 

	* After selecting one of these two machines, it is possible to select multiple machines From / To that flows to or from the selected machine are connected to more than one From/To Machine.
	
	* In the case where the MP (raw materials) / FP (finished products) are not shown if the "old machine" is the first machine through which pass operations, IN option (entry point) will be the only available in the From box. Conversely, if the "old machine" is the latest machine through which pass the operation, the OUT (exit point) will be the only available option in the To box.


* If all the boxes are green, click on "OK" and the flow will be immediately reassigned

Remarks :

	* It is not possible to reassign the flows to the baseline (Base). To perform an rerouting, you will first duplicate this scenario. 

	* If after selecting the "old machine", no machine is present in the "new machine" list, then there is no other machine of this type available to reassign the flows. In this case, it will be necessary to duplicate the first "old machine".
	
	* The machines listed in the From and To are dependent on one another. Selecting one of these two machines will cause an update of the opposite if no machine had been selected in this list. To reset both lists, you must click on the white box located at the top.


Options / Filters
------------------

.. pipe box_filter pipe

Products
~~~~~~~~~~~~~~~~

|box_filter_product| 

The menu of filters by product allows display only part of the graphical flows. It is possible to perform a filter:

* Based on a selection of product: Each product is characterized by its proportion of parts and transactions.

|box_filter_product_product|

* Based on the product families: first, it is necessary to select the alternative that identifies families to view. These families will be proposed only if cells were identified in the alternative.

|box_filter_product_family|

* Based on a Pareto of the product  transactions: Allows you to filter the products based on a percentage of items / transactions.

|box_filter_product_pareto|

Information dispplay :

|box_filter_product_info|

* **Quantity of products** : Shows the number and percentage of products / parts mentioned in the filtered products.

* **Quantity of transactions** : Shows the number and proportion of transactions affected by the filtered products.

* **Total number of product references** : Indicates the total number of products affected by the filtered products. 



Machines
~~~~~~~~~~~~~~~~

|box_filter_machine| 

The filter menu by machines is used to display only part of the graphical flow. It is possible to filter based on a selection of machines. Only selected machines will be represented on the graph.
You can (de) select all machines at once using the "select all" button.



Optimizer/Cell Optimizer
-------------------------------------------------


= Generation of optimized solutions

 When you click on the "Cell Optimizer" option in the  "Optimizer" menu, a box appears to select the scenario to which the "Cell Optimizer" will be applied.

|box_optimizer_open|

Cell Optimizer allows the automatic generation of optimized solutions. 
To do this, you must first select the scenario on which we want to base the generation of solutions.
 

Once selected, a new screen will appear where three proposals are proposed. The first solution, which best matches the current situation, will be selected. The other two proposals are generating variants of this. 

|box_optimizercell|

Once the base scenario has been selected, it is possible to chang the following parameters for each proposal:

* The number of duplicates of each machine (machines x  nb Occurences)

* The number of cells.

* The maximum number of machines by cell.

* Whether or not the MP (raw materials) / FP (finished products) must be found within the cells. (Caution: MP / PF are not included in the maximum number of machines per cell, there will be a much torque against MP / PF as there are cells).

In the event that another scenario is selected, it will be possible for each proposal, to change the following parameters:

* The number of cells.

* The maximum number of machines by cell.

* Whether or not the raw materials MP / PF finished parts must be found within the cells (the number of MP / PF will not be dependent on the number of cells).

.. hint:: In this second case, the machines taken into effect in the generation of the solution will be dependent on the machine duplications made by the user to the selected scenario.

* When the solutions are created, they can be applied on a scenario

|box_applysolution|

	* Select the optimized solution to apply
	
	* Select the scenario on which the solution will be applied

	* Specify if the scenario must be duplicated 

.. hint:: The cell solution is applied on the graphical view. The routing solution (operation assigned on machines) is applied to all the views of the scenario. The machines location on the real view will not be changed.