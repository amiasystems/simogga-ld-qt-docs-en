.. include:: image.rst
.. include:: icon.rst

Main menu
===============


SIMOGGA
---------


File
--------
 
|menu_file|

* **Open** : Open a case on the basis of a data xml file.

|menu_file_open|

* **Save** : Save the case by changing the data file loaded.

|menu_file_save|

* **Save As** : Save by allowing to choose the name and location of the xml file.

|menu_file_save_as|

* **Close** : Close the opened case. It will first ask it a backup of any changes made to the current case is necessary.

|menu_file_close|


* **Exit** : Closes the application. It will first ask if a backup of any changes made to the current case is necessary.

|menu_file_quit|

Scenario
-----------

|menu_scenario|
 
* **New Scenario** : Creates a new blank scenario with an alternative of each type.

|menu_scenario_new_sce|

* **Duplicate Scenario** : Duplicates the current scenario.

|menu_scenario_dup_sce|

* **Delete Scenario** : Removes the current scenario and all its alternatives.

|menu_scenario_del_sce|

* **New Alternative** : Creates a new blank alternative.

|menu_scenario_new_alt|

* **Duplicate Alternative** : Duplicates the current alternative. 

|menu_scenario_dup_alt|

* **Delete Alternative** : Delete the current alternative.

|menu_scenario_del_alt|

* **(Un)Select the alternative as reference** The common alternative is stored as an alternative reference at the dashboard. One alternative may be indicated as a reference. All other alternatives will be compared to the alternative reference.

|menu_scenario_as_is|

.. Set/Unset Alternative as Baseline

Options
---------

|menu_option|
 
* **Configuration Panel** : Displays the panel containing the customizable settings SIMOGGA.

|menu_option_conf|

* **Rerouting** : Displays the panel to proceed rerouting products to change the flow between machines. This can not be done on the baseline (Base).

|menu_option_conf|

* **Filters** : Displays the panel to make filters on products and machines.

|menu_option_filter|

Optimizer
------------

|menu_optimizer|

* Cell Optimizer : Display panel for generating independent cells. 

|menu_optimizer_cell| 

* Apply Solution: Apply an optimized solution to the current alternative specifying whether the scenario must be duplicated or not. 

|menu_optimizer_apply|

Languages
--------------------

Choice of languages. 
Available languages: French, English, German.

Help
---------

Access to the detailed help
