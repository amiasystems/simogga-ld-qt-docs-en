Menu "Views"
====================

.. include:: image.rst
.. include:: icon.rst


Machines
-----------

Characteristics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|machine|

* **Colour** : The color defines the type of the machine.
* **Load** : Filled color represents the percentage of load relative to the available capacity.
* **Border** : The contour of the machine sets the floor area required for placement.

Machine Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In addition to their movements using |move|, other interactions with the machines are possible, regardless of the current "view".

Possible Options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* **right click** : 

	* **Duplicate** : Used to duplicate a machine. The duplicate machine appears next to the original. This action is not possible in the baseline scenario.

	|machine_rc_base|

	To duplicate a machine, it is first necessary to duplicate this scenario. 

	|machine_duplicate|

..	* **Rotation** : Enables the machine to rotate 90 °.

..	 pipe machine_rotate pipe

	* **Delete** : Delete the alternative machine.

	|machine_delete|

	It is only possible to delete the duplicated machines.

	|machine_rc_ref|
	
..	* **Display IOs** : Shows the input and output of the machine. This option is useful when the machine is very big.
..  pipe machine_io pipe
 
..	* **Lock the machine** : Locks the machine in its position on the plan.

..	pipe machine_locked pipe
 
* **Resize** : When the machine is selected, you can resize the outline defining the used floor space. With the mouse, move one of the eight spots on the machines outline.

|machine_resized|

.. Flux
.. -----------

.. Cellules
.. -----------

.. Zones
.. -----------

.. Squelette d'allée
.. ----------------------
