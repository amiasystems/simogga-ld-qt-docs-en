Menu "View"
==============

.. include:: image.rst
.. include:: icon.rst

Graphical View
--------------

|panel_graphic|

* |multiple_selection|  **Multi-selection** : Select multiple machines simultaneously with a selection polygon drawn by the user (click around machines and double click to close the contour).


* |delete_item|  **Delete object** : Deletes an item. In the graphical view, it is only possible to delete a virtual cell.


* |empty_machine|  **Show/Hide empty machines** : Allows you to hide or display machines that are not used.


* |connect|  **Connect/Disconnect the machines** : Hide or display the flows passing between machines.


* |prevent_collision|  **Prevent collision** : Prevents two machines from overlapping.


* |removeMachineFromVirtualCell|  **Free machines** : Allows machines no longer be locked within the cell in which they are located.


* |input|  **Add IN/OUT** : Adds an input / output to a virtual cell by double-clicking on one of the edges of the cell.


* |lock_machine|  **Lock/Unlock machine** : Will lock a machine. This machine can therefore cannot be moved.


.. * |rotate_machine|   **Rotate machine** : Enables the rotation of a machine.


* |insert_virtual_cell|  **Add virtual cell** : Adds a virtual cell which takes the form of an expandable rectangle. These virtual cells will define product families.


* |table_of_families|  **Show ProdByCell tab** : Displays the table containing the products associated with each virtual cell.



Blueprint View - Interaction Mode
------------------------------------------

|panel_real_interaction| 

* |delete_item|  **Delete object** : Deletes an object. Within the "real" view in interaction mode, it is only possible to remove input / output points.

* |hide_background|  **Show/Hide background** : Can hide or display the background image that was selected.

* |panel_machine|  **Show/Hide machine panel** : Can hide or display the tray containing the machines do not currently located within the plant.

* |empty_machine|  **Show/Hide empty machines** : Allows you to hide or display machines that are not used..

* |connect_machine|  **Connect/Disconnect the machines** : Allows you to position the machines on the plan without them connected to a flow. Interesting option when the refresh is too slow. At the end of the placement of all machines, return to "Connect" position to bring the situation to date.

* |prevent_collision|  **Prevent collision** : Prevents two machines from overlapping.

* |lock_machine|  **Lock/Unlock machine** : Will lock a machine. This machine can therefore cannot be moved.

* |rotate_machine|  **Rotate machine** : Enables the rotation of a machine.

.. * |multiple_selection|  **Multi-selection** : Select multiple machines simultaneously using a selection designed by the user.

* |visualisation_standard|  **Visualization** : Allows to choose between different vizualisation of the traffic.

|visualisation_type|
	
	* |visualisation_standard| **Visualization standard** : Connect directly all machines inside the same cell. Otherwise, the connection is made by the aisles thank to the shortest road.

	* |visualisation_byalleys| **Visualization by aisle** : No direct connection between machines. All traffic go through the aisles.

	* |visualisation_crowfly| **Visualization as crowfly** : All machines are connected via a direct connection. This visualization allow the directional traffic.


Blueprint View - Design Mode
------------------------------------------

|panel_real_design| 

* |move|  **Move element** : Move an object


* |delete_item|  **Delete object** : Allows deletion of an object or multiple (the Ctrl / Cmd key) objects selected. 


* |insert_factory_plan|  **Add background** : Allows adding a background image to trace the factory plan.


* |scale|  **Set scale** : Changes the scale of the factory. The user can draw a line (click on each end) and define it's length in meters.


* |insert_factory_border|  **Create zone** : Allows the creation of an area (Site, Building, Floor or Area) a polygon that the user will be able to draw. Click on the map to locate the contour points. To close the box, double-click on the last point of the contour. There is no need to double click on the first point. If the area is within a parent zone (first point in a parent area), it will automatically be truncated so that their boundaries correspond to the latter.


* |insert_cell_border|  **Create cells** : Allows the creation of a cell represented by a polygon within an area. Only that area may contain cell machinery. If the cell is within a parent zone, it will automatically be truncated so that their boundaries correspond to the latter. It is not useful to follow the contour of the plant with precision.

 |design_cell_view| =>  |design_cell_tronq_view|

* |insert_skeleton_point| **Add skeleton point** : Adds a skeleton point by double-clicking on the selected area. If other points are already in this area, the connection between these points, if it is possible (line internal connection to the comings and not "too close" to the corners of the cells) will happen automatically.


* |insert_edge|  **Add skeleton line** : Allows you to manually add a skeleton line by double-clicking on the two points that compose it.


* |input|  **Add IN/OUT** : Adds an input / output to an area by double-clicking on one of the edges of this area.


* |nodes|  **View skeleton** : Displays the diagram with interconnections. This graph is the basis for the traffic display.


.. * |nodes|  **Show/Hide nodes** : Can hide or show the connection nodes to the graph. 

