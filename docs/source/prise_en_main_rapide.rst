Quick Start Guide
======================

.. include:: image.rst
.. include:: icon.rst

|prise_en_main|


Excel File
-----------------

To collect the data from the customer and to simplify the completion of the xml, an excel file is used: `AMIA-SIMOGGA-SampleCase.xlsm <https://www.amia-systems.com/wp-content/uploads/2019/11/1.AMIA-SIMOGGA-XML-v2.7.3.xlsm.zip>`_

The excel file with the data for the Trial Version can be download `here <https://www.amia-systems.com/wp-content/uploads/2019/11/1.AMIA-SIMOGGA-XML-v2.7.3.xlsm.zip>`_

Per the instructions provided in the excel file, only the columns highlighted in yellow are required to be completed. All of the other columns are for information to enable a more in-depth analysis (such as machine load and capacity).

A macro converts the Excel file into xml file readable by SIMOGGA.

A file « SIMOGGA-output.xml » is created in the repository beginning by XLS2XML (at the same level as the excel file).



Creating of the As-IS situation in the **interaction view**
-----------------------------------------------------------

Opening a file
~~~~~~~~~~~~~~~~~~~~~~~~~~

To start working with SIMOGGA, two options are possible:
- Create a new case in the "File > New" menu. It create a new case whithout any data, you can fill all the data on the data panel.
- Open an existing file and there you have also two choices.
 - Open a file generated from Excel. It is a .xml file.
 - Open an already worked on case in SIMOGGA. It is a .db file that you can open in the "File > Open" menu or in the "File > Recent files" menu. 
 
|open_file|

When opening a file, a dialog box appears asking whether SIMOGGA must create locations for Raw Material (MP) and Finished Part (FP). If the data already includes these inputs and outputs of the system, click on "No".

For the TrialCase, click on "Yes".  The representation of the flow is more complete if we take into account the inputs and outputs of the system. 

|raw_material|

Opening a file starts in the ‘design’ view. You must first draw your factory, and define the working(yellow cell). Once it is done you can go to the 'interaction' view.
By default, the machines are placed in a tray. 


Saving a case
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two types of save options can be found in the "File" menu:

* Save: The backup will be done on the open file. The changes will be saved in a db file.

* Save As ...: The user can choose the backup location and name of the saved file.

.. hint:: Don't forget to save your work regularly to avoid any lost.


Create the factory - Design view
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For a scenario it is possible to obtain a two-dimensional representation of the work area corresponding to the reality.

* Save this scenario as the reference: Click on 'Scenario' on the and select "Set / Unset as Baseline" option. This will serve as a benchmark to compare all scenarios. 

|set_as_baseline|

* |add_blueprint|   Insert a factory blueprint as a background image  
* Adapt the size of the map (via the zoom with the mouse wheel) in function of the sample machine. |sample_machine|
 
* |set_scale|  Set the scale of the factory plan.
 
* |insert_zone|  Draw the outline of the factory on the plan. 
  * With the freeform mode, click to draw the factory borders and double click to close it. 
  * With the rectangle zone, click to fix a border of the rectangle, drag and drop to draw the zone.
 
* |insert_cell| Draw the working areas (cells). To draw a cell, click first on it's parent zone and then click on the insert cell menu. 
When a cell is drawed out it's parent zone, the borders will be croped.
|crop_cell|

* To move a zone, just drag and drop it.

* |resize_zone| To resize a zone, click on the zone and a little square will appear. Drag and drop the square and the zone will be resized.
 
* |turn_zone| To turn a cell, click on the zone and a little circle will appear. Drag and drop the circle and the zone will turn 90°.

* |cell_context_menu| By a right click on a zone, a context menu shows more actions on this zone.


Creation of the aisles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The skeleton of the aisles is a graphical representation of paths by points and lines.
	
* |insert_skeleton_point|  Insert the skeleton points by double clicking on the intersection of the aisles. These points represent the connection points of the aisles.
	
* Automatic interconnection points in order to create the aisle lines	

* |insert_edge|  Create connections between two unconnected points automatically by double-clincking on both points to connect
	
* |input|  Insert the In/Out (IO point) on the zone border with double clicking  

* |insert_edge|  Define the aisles in the working area by successive double-clicking 
		
* |line_context_menu| With a right click on a line, set it's distance or set this line as signle or double way.



Positioning of the machines – Interaction Mode 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* |change_mode_interaction| Select the "Interaction Mode" in the menu.

* Move the machines from the tray to the working cells in the current location of the factory (by clicking CTRL / CMD you can select multiple machines). They will be automatically connected to the aisles. If there is a flow between two machines positioned on the plan, it will appear automatically.

* Choose the visualization

	* |visualisation_standard| Standard : Machines are connected directly if they belong to the same area, if not traffic is through the aisles.

	* |visualisation_byalleys| By the aisles only: All traffic via the aisles.

	* |visualisation_crowfly|  As the bird flies: The aisle backbone is not used. The machines are connected via directional flow.

	.. hint:: In the first two displays, the concept of direction disappears. It is all the traffic that is shown on each segment. This type of view mode is only possible if an aisle skeleton was built in design mode. Otherwise, the representation is as the crow flies.

* |machine_context| The machines can be modified by:

	* The position with "drag & drop"
	* Their size to represent more accurately the real place that takes the machine within the factory
	* Their orientation
	* Their immobilisation. A locked machine canot be moved.
	* Their in/out. You can choose where the flow enter to the machine and from where the flow go out the machine.
	

Dashboard
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* |dashboard| When all machines are in position, the performance indicators assess the situation quickly

	* Number of kilometers traveled
	* Travel time
	* Cost of transport


Reorganisation
~~~~~~~~~~~~~~~~

Tools available:

* The filters by machine allow the analysis of those flows connected to one or more machines.

|filter_by_machine|
 
* Product filters visualize the flows of one or more products. 

|filter_by_product|

In the selection screen, each product is represented by its flow rate (in terms of quantity or transactions).

|filter_by_product_prod|

* Filters by product family visualize flows for one or more product categories when selecting a prior alternative

|filter_by_family|

* A Pareto filter can be used on the quantities of goods transferred or the number of transfers made (transactions). 

|filter_pareto|

These filters will focus the analysis on some of the products

All information about the filtered products are available in the below of the windows (Number of parts, number of transactions)

|filter_info|

Making a flow analysis on a Pareto of products could bias the study. 
The tools available in SIMOGGA allow for the analysis of all the products.
	
* A visual filter takes into account all the products, but displays only the flows between a minimum value (bottom filter) and a maximum value (upper filter).

|visualisation|

* Focus the analysis on a segment of value with a minimum and maximum value and look with a magnifying glass by checking "Magnify". Flows will differentiate from each other.


Evaluation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The creation of the cells will allow SIMOGGA to create product families assigned to these cells.
The products are allocated to a cell by the number of transactions inside a cell. They are displayed in different colors (from green to red) based on the relevance of membership to the cell.

This step is useful to know the directional flow between cells that we would like to create in the real view.
The intracellular flux (inside the cells) are measured and represented as a percentage of total flows.

|table_of_families|


Scenario Generation
--------------------------

Based on the situation AS-IS and analysis of previous flows, it is possible to create all possible scenarios.  

.. ::hint A secnario has a unique design, a set of defined machines and a flow matrix (a routing solution where each operation is assigned to a machine).

A scenario is used to test a new design (new working area, new aisles...), solutions with new investments in term of machines or new routings for the parts.
It's also used to test the location of machines.

To change the positions of the machinery from the As-Is situation 

* Duplicate « Scenario1»
* Changing the positions of the machines has a direct impact on the mileage (in green if a gain is observed)

|evaluation_positive|

|evaluation_negative|

* Analyze the Dashboard

|use_scenarios|

To create solutions based on the rerouting of flows performed in the graphical view of the "Scenario 2":

* Work in Scenario 2 alternative "S2RA1"

* Determine incremental changes (in terms of positioning of machines):
	* Modify « S2RA1 »
	* Duplicate« S2RA1 » en « S2RA2 »
	* Modify « S2RA2 »
	* Duplicate « S2RA2 » en « S2RA3 »
	* Modify « S2RA3 » 

..hint each stage to be saved must be duplicated in order to keep the changes.

* Modify the design of the factory
	* Duplicate Scenario 2 (Scenario 3 =)
	* Modify the design (delete cells, create new cells ...)
	* Delete the non-useful alternatives from the previous Scenario 2 ("S2RA1" and   "S2RA2")
	* Start to change the alternative "S3RA3" (new design with machines positioned as the last alternative scenario 2)
	* Proceed with incremental changes to create a different alternative "S3RA3", "S3RA4" to "S3RA5"

Incremental changes (via alternatives) can result in freeing up of an area so as to facilitate redesign. (via a new scenario).

Close
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|menu_file_close|

When closing the opened case with the command "Close" in the menue "File", it will first ask it a backup of any changes made to the current case is necessary.

|message_close|

If the response is "Yes", the modification will be saved in the original file xml. On the contrary, the modification will be lost. 

