Planner Guide - Configuration
=============================

.. include:: image.rst
.. include:: icon.rst

Basis of the Planner
------------------------

SIMOGGA Planner comes as an extension of SIMOGGA Layout Design. In order to work, it doesn't need more than the data that have already been used to design the as-is and optimized situation of the factory.
However, to obtain a more complete and accurate scheduling, configuring the Planner will be required. The following paragraphs will explain in details how to take all the constraint and the caracteristics of the production into account.


Configuring the Planner
-------------------------

The Global Data
~~~~~~~~~~~~~~~~

The global tab will impact general parameters for the schedule.

|scheduler_menu_global_1|

In this first part of the menu, it is possible to set the 'global' values for the schedule. These values will be taken into account if, for an operation, no value has been set on both the operation and machine level. This is, in other words, a default value.
It is also possible to only use these default values for every operation to be done on the schedule. In order to do that, simply check the "Use global values over machines and operations ones" in the second part of this tab.
For each kind of sub-operation, you can also set if the sub operation needs an operator or not.

|scheduler_menu_global_2|

* **Nb Operators** will determine the number of operators available inside the factory. These operators are independant from the ones dedicated to a specific machine and won't be working on these machines either. 
 
* **Use global values over machines and operations ones** will make the Planner only take into account the global values set in the first part of the tab.

* **Allow changes to machines assignation** will make rerouting possible if duplicate machines exists. If the first machine is busy, the next products requiring it will be moved to the second/... machines.

* **Schedule only filtered products** will limit the schedule to the products currently filtered. This means that you can schedule only a part of the production or see what would be the impact on the production by removing one or multiple products from it.

|scheduler_menu_global_3|

* **Precision** determines if the schedule will be done in seconds, minutes, or hours

* **Start on** is the day and the hour at which the schedule will start

* **End On** is the day and the hour at which the schedule must end. It will end even if all the operations were not scheduled

* **Allow process to continue out of shift** will allow a process that has been started before the shift ends to continue after the end of the shift. If this is not checked, no operation that cannot be finished before the end of the shift will be started.

* **Batchs cannot be separated** will make the batchs indivisible, which means the Planner will always wait that all the units of the batch are done before starting the operations on the next machine.

* **Max number of concurrent orders** is the number of orders that can be treated simultaneously inside the factory. If no number is set, it is infinite.
    * Prioritize orders over bottleneck machine means that if one machine is set as the bottleneck (which will also limits the number of orders inside the factory), the limit set here will have the priority.

The Shifts Data
~~~~~~~~~~~~~~~~

|scheduler_menu_shift|

* **Shift(s)** sets the shift schedule of the factory. At this moment, four options are possible
    * No shift at all
    * 1 shift from 8am to 4 pm
    * 2 shifts from 6am to 2pm and from 2pm to 10pm
    * 3 shifts from 6am to 2pm and from 2pm to 10pm and from 10pm to 6am

* **Saturday** and **Sunday** will determine whether the factory is running on saturday and sunday

The Machines Data
~~~~~~~~~~~~~~~~~~

|scheduler_menu_machine_1|

* The first part of this tab is the same than the one from the global tab. It will determine for each machine the time for each sub operation done on that machine. In case no value is defined at the "operation" level, the Planner will take this value into account.
* A **dedicated operator** can be set to the machine. It means this operator will take care of that machine and the operators defined in the global tabs won't work on it. Note that it is possible to select multiple operators to work on one or multiple machines. For example, you could set the dedicated operators 1 and 2 on the machine A and the dedicated operators 2 and 3 on the machine B. It means that the dedicated operator 2 will be working specifically on both machine A and B.
* If the **Use machines values over operations ones** is checked, the Planner will take the values defined for each machine into account not matter what is set in the operation section.

|scheduler_menu_machine_2|

* A **specific shift** can be set for each machine, including if it works on saturday and sunday
* The machine can be set as the **bottleneck machine**. It means that the Planner will try to optimize the schedule and avoid creating a bottleneck on the specified machine by delaying the start of the orders that might cause it.
* It is possible to specify that **the machine can treat multiples products simultaneously**. 
    * In this case, it is necessary to specify the amount of products to be treated simultaneously.
    * If "Nb is fixed" is checked, it means that the Planner will always wait to have to total amount our product ready before starting the operation. If it is not checked, the operations might start with one or some products even if there is less available than the number specified.

The Operations Data
~~~~~~~~~~~~~~~~~~~~

|scheduler_menu_operation_1|

* Just like for the global and machine tabs, it is possible to set the time spent of each sub operation of the given operation and if an operator is needed. Unless specified otherwise by the users, the values setted in the operation tab will be the ones used by the Planner to make the schedule.
* It is possible to modify the **transfer size** of the operation. It means the amount of product transfered simultaneously to the next machine (operation).

|scheduler_menu_operation_2|

The Products Data
~~~~~~~~~~~~~~~~~~

|scheduler_menu_product|

* It is possible to change the **batch size** for each product and sub product. This can be interesting in order to test which batch size is the more adequate given the production.

The Orders Data
~~~~~~~~~~~~~~~~~~

|scheduler_menu_order_1|

* Once the schedule has been prepared, it is possible to generate the list of orders. Once the orders are created, it is possible to edit their release and due date.
* If no **release date** is set, the Planner will consider the order available right from the start of the schedule.
* If a **release date** is set, the order will not be able to start before the schedule has reached the set release date.
* There is multiple ways to set the **due date**. It can either be :
    * At a fixed date.
    * After a specified number of days that can either be :
        * Since the release date.
        * Since the beginning of the schedule.
    * No due date at all.


|scheduler_menu_order_2|

* It is possible to specify the **type of ordering** (priority) for the Planner. Multiple choices are available :
    * **By due date** which is the most common. The planner will start the orders based on the closest due dates in order to avoid as much as possible late delivery.
    * **By product id**. The priority will be determined by the id of the product. This is mainly used for specific test cases or testing.
    * **Randomized**. The orders will be launched randomly. This is useful when the production varies continuously and the orders arrive randomly.
    * **Smart**. Currently in alpha. The goal of this method is to directly prevents bottlenecks and have the best balance between lead time and minimum lateness.
