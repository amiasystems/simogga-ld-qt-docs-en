# Planner - Results
===================

.. include:: image.rst
.. include:: icon.rst

## Summary tab
----------------------

KPIs
~~~~~
The first tab displays a comparative table of all the schedules launched. It contains several sets of data.

!! KPI1.png

* **Name** is the name of the schedule. It is a combination of the scenario, the real view and the schedule number.
* **Makespan** is the total amount of time for the schedule in minutes.
* **Average machine use** is the average percentage of machine utilization.
* **Average lead time (orders)** is the average lead time for the orders in minutes.
* **Average lead time (pieces)** same as above, but for each piece.
* **Percentage of touch time compared to lead time (units)** is the average percentage of effective time spent on operation for each unit compared to the time it actually spent inside the factory.
* **Average wait time** is the average wait time for each operation.
* **Percent of wait time because of sub products** is the percentage of wait time caused by sub products not ready on time to be used by the main products.
* **Percent of wait time because of operators** is the percentage of wait time due to the fact that there were no available operators when the piece was ready to be processed.
* **Total wait time because of operators** is the total amount of wait time (in minutes) due to the fact that there were no available operators when the piece was ready to be processed.
* **Average lateness** is the average lateness for each order of the schedule. A negative value indicates that the order are on average done before the due date. Whereas a positive value indicates the orders are on average done after the due date.
* **Total lateness** is the total lateness in minutes for all the orders.

Next to each name there is a checkbox. Checking it will make the corresponding schedule a reference the others schedules are compared to.

!! KPI2.png

For each category, better results for other schedules will be shown in green and worse results in red. And if it is exactly the same value, it will be indicated in grey, the same colour as the reference schedule.

Relative wait time
~~~~~~~~~~~~~~~~~~
There are two graphs but they display the data in the exact same way. 
* Wait time because of operators
* Wait time because the subproducts were not available on time.

###### !! relativeWaitTime.png (not yet screenshot, dashsboard still needs to be updated.)

Each of the donuts represents a different schedule.
The values in those two graphs are not absolute but relative to a reference schedule. The referenced schedule is in red and its value (total wait time due to either the operators or the subproducts) represents 100%. The other schedules are represented by a percentage that is calculated as the ratio between the amount of wait time due to either cause of this schedule and the wait time of the referenced schedule. Thus the result of any schedule that is not a reference is between 0 and 100% since the schedule having the most wait time is the refence one.
###### !! relativeWaitTimeHovering.png (not yet screenshot, dashsboard still needs to be updated.)

When hovering over any slice with the mouse cursor, the slice will be highlighted and its value will be display next to it.

Makespan
~~~~~~~~~
It displays the total makespan (in hours) for each schedule.

##### !! makespan.png

Average lateness
~~~~~~~~~~~~~~~~~
For each schedule it shows the average lateness per order.

###### !! averageLateness.png (not yet screenshot, need a screenshot with positive lateness as well.)

There is a blue line representing the due date whose value is 0 going through the graph. For each schedule, if the lateness is negative (in advance), it is represented by a green bar. If the lateness is positive (late) the data is displayed in red. Each time the exact value is indicated next to the schedule number at the bottom of the chart.

Total lateness
~~~~~~~~~~~~~~
It is the same as the previous one execpt it now shows the total lateness for each schedule.

###### !! totalLateness.png (not yet screenshot, need a screenshot with positive lateness as well.)

Average use
~~~~~~~~~~~~

Average lead time (orders)
~~~~~~~~~~~~~~~~~~~~~~~~~~
It displays the average lead time in minutes per order for each schedule.

##### !! avLeadTimeOrd.png

Average lead time (pieces)
~~~~~~~~~~~~~~~~~~~~~~~~~~
It displays the average lead time in minutes per piece for each schedule.

##### !! avLeadTimePiece.png

Number of setups done
~~~~~~~~~~~~~~~~~~~~~~
This graph shows the number of setups done. For each schedule, it indicated whether the number of setups is higher or lower than the number of setups of the initial schedule. 

##### !! nbSetupsDone.png

If the difference (between the current schedule's number of setups and the initial one) is positive, a red bar shows how many extra setups were needed. The value is indicated next to the schedule number at the bottom of the chart.
If the schedule required less setups, a green bar represents the difference in the number of setups.
However if a schedule required exactly the same number of setups as the initial one, this schedule is represented by a blue bar displaying the total number of setups. And the mention *"same"* appears next to the schedule's number.  


